﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ColorPlatformer
{
    [CustomEditor(typeof(ColorComponent))]
    public class ColorComponentInspector : Editor
    {
        SerializedProperty m_InitializeOnAwakeProp;
        SerializedProperty m_UpdateNameProp;
        SerializedProperty m_SpriteRendererProp;
        SerializedProperty m_ChangeLayerProp;
        SerializedProperty m_CurrentColorProp;

        //private COLOR _CurrentColor;
        private ColorComponent _Target;
        private MonoScript script = null;

        protected virtual void OnEnable()
        {
            // Fetch the objects from the GameObject script to display in the inspector
            m_InitializeOnAwakeProp = serializedObject.FindProperty("_InitializeOnAwake");
            m_UpdateNameProp = serializedObject.FindProperty("_UpdateName");
            m_SpriteRendererProp = serializedObject.FindProperty("_SpriteRenderer");
            m_ChangeLayerProp = serializedObject.FindProperty("_ChangeLayer");
            m_CurrentColorProp = serializedObject.FindProperty("_CurrentColor");

            // Save the target
            _Target = (ColorComponent)target;

            // Add the own script
            script = MonoScript.FromMonoBehaviour(_Target);
        }
        
        public override void OnInspectorGUI()
        {
            bool colorChanged = false;
            script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(m_CurrentColorProp, new GUIContent("Current Color"));
            if (EditorGUI.EndChangeCheck())
            {
                colorChanged = true;
            }
            EditorGUILayout.PropertyField(m_UpdateNameProp, new GUIContent("Update Name"));
            EditorGUILayout.PropertyField(m_InitializeOnAwakeProp, new GUIContent("Initialize On Awake"));
            EditorGUILayout.PropertyField(m_SpriteRendererProp, new GUIContent("Sprite Renderer"), true);
            EditorGUILayout.PropertyField(m_ChangeLayerProp, new GUIContent("Change Layer"));

            serializedObject.ApplyModifiedProperties();

            // If the color has changed, change the game object
            if (colorChanged)
            {
                _Target.UpdateColorAndLayer();
            }
        }
    }
}
