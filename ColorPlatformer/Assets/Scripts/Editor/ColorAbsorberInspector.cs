﻿using UnityEditor;
using UnityEngine;

namespace ColorPlatformer
{
    [CustomEditor(typeof(ColorAbsorber))]
    public class ColorAbsorberInspector : ColorParticleComponentInspector
    {
        private SerializedProperty _RotationAxisProp;
        private SerializedProperty _SpeedProp;

        protected override void OnEnable()
        {
            base.OnEnable();
            _RotationAxisProp = serializedObject.FindProperty("_RotationAxis");
            _SpeedProp = serializedObject.FindProperty("_Speed");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.PropertyField(_RotationAxisProp, new GUIContent("Rotation Axis"));
            EditorGUILayout.PropertyField(_SpeedProp, new GUIContent("Speed"));

            serializedObject.ApplyModifiedProperties();
        }
    }
}
