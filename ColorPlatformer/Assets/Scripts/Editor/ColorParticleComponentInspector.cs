﻿using UnityEditor;
using UnityEngine;

namespace ColorPlatformer
{
    [CustomEditor(typeof(ColorParticleComponent))]
    public class ColorParticleComponentInspector : ColorComponentInspector
    {
        private SerializedProperty _ParticleSystemProp;

        protected override void OnEnable()
        {
            base.OnEnable();
            _ParticleSystemProp = serializedObject.FindProperty("_ParticleSystem");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.PropertyField(_ParticleSystemProp, new GUIContent("Particle System"));

            serializedObject.ApplyModifiedProperties();
        }
    }
}
