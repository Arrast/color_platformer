﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class GameObjectDuplicator : MonoBehaviour
{
    [MenuItem("VERSoft/Tools/Duplicate Selected and move left %#LEFT")]
    public static void DuplicateAndMoveLeft()
    {
        DuplicateAndMove(Vector2.left);
    }

    [MenuItem("VERSoft/Tools/Duplicate Selected and move up %#UP")]
    public static void DuplicateAndMoveUp()
    {
        DuplicateAndMove(Vector2.up);
    }

    [MenuItem("VERSoft/Tools/Duplicate Selected and move right %#RIGHT")]
    public static void DuplicateAndMoveRight()
    {
        DuplicateAndMove(Vector2.right);
    }

    [MenuItem("VERSoft/Tools/Duplicate Selected and move down %#DOWN")]
    public static void DuplicateAndMoveDown()
    {
        DuplicateAndMove(Vector2.down);
    }

    private static Dictionary<float, List<GameObject>> selectedGameObjectsByPosition;
    public static void DuplicateAndMove(Vector3 vectorToMove)
    {
        Vector3 absolute = new Vector3(Mathf.Abs(vectorToMove.x), Mathf.Abs(vectorToMove.y), Mathf.Abs(vectorToMove.z));
        GameObject[] selectedGameObjects = Selection.gameObjects;
        GameObject[] newGameObjects = new GameObject[selectedGameObjects.Length];

        selectedGameObjectsByPosition = new Dictionary<float, List<GameObject>>();
        if (selectedGameObjects != null)
        {
            //float distance = 0;
            for (int i = 0; i < selectedGameObjects.Length; i++)
            {
                // We have to find the furthest point in the direction we are looking
                if (absolute.x > 0)
                {
                    AddElementToDicionaryList(selectedGameObjects[i].transform.position.y, selectedGameObjects[i]);
                }
                else
                {
                    AddElementToDicionaryList(selectedGameObjects[i].transform.position.x, selectedGameObjects[i]);
                }

                // We create the new copy
                GameObject duplicateInstance = null;
                GameObject prefabRoot = PrefabUtility.GetCorrespondingObjectFromSource<GameObject>(selectedGameObjects[i]);
                if (prefabRoot != null)
                {
                    duplicateInstance = (GameObject)PrefabUtility.InstantiatePrefab(prefabRoot);
                }
                else
                {
                    duplicateInstance = Instantiate(selectedGameObjects[i]);
                }

                duplicateInstance.transform.SetParent(selectedGameObjects[i].transform.parent, false);
                duplicateInstance.transform.localScale = selectedGameObjects[i].transform.localScale;
                duplicateInstance.name = selectedGameObjects[i].name;
                newGameObjects[i] = duplicateInstance;

                // We record them for undo
                Undo.RegisterCreatedObjectUndo(duplicateInstance, "Duplicated Items");
            }

            // We calculate the new position
            float distance = CalculateMaxDistanceForGameObject(absolute);
            for (int i = 0; i < newGameObjects.Length; i++)
            {
                newGameObjects[i].transform.position += distance * vectorToMove;
            }

            // We set the new gameObjects as selected
            Selection.objects = newGameObjects;
        }
    }
    
    public static float CalculateMaxDistanceForGameObject(Vector3 vectorToMove)
    {
        float distance = int.MinValue;
        foreach (var key in selectedGameObjectsByPosition.Keys)
        {
            float temp = 0;
            for (int i = 0; i < selectedGameObjectsByPosition[key].Count; i++)
            {
                // Store the renderers.
                SpriteRenderer renderer = selectedGameObjectsByPosition[key][i].GetComponent<SpriteRenderer>();
                temp += (renderer.bounds.size.x * vectorToMove.x + renderer.bounds.size.y * vectorToMove.y);
            }

            distance = Mathf.Max(distance, temp);
        }
        return distance;
    }

    private static void AddElementToDicionaryList(float position, GameObject gameObjectToAdd)
    {
        if (!selectedGameObjectsByPosition.ContainsKey(position))
        {
            selectedGameObjectsByPosition[position] = new List<GameObject>();
        }
        selectedGameObjectsByPosition[position].Add(gameObjectToAdd);
    }
}
