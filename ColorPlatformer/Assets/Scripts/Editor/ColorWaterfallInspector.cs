﻿using UnityEditor;
using UnityEngine;

namespace ColorPlatformer
{
    [CustomEditor(typeof(ColorWaterfall))]
    public class ColorWaterfallInspector : ColorComponentInspector
    {
        private SerializedProperty _AnimationAxisProp;
        private SerializedProperty _SpeedProp;

        protected override void OnEnable()
        {
            base.OnEnable();
            _AnimationAxisProp = serializedObject.FindProperty("_AnimationAxis");
            _SpeedProp = serializedObject.FindProperty("_Speed");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.PropertyField(_AnimationAxisProp, new GUIContent("Animation Axis"));
            EditorGUILayout.PropertyField(_SpeedProp, new GUIContent("Speed"));

            serializedObject.ApplyModifiedProperties();
        }
    }
}