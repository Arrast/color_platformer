﻿using ColorPlatformer.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace ColorPlatformer
{
    [CustomEditor(typeof(LevelManager))]
    public class LevelManagerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            LevelManager targetLM = (LevelManager)target;
            if (GUILayout.Button("Fetch All GameplayMonoBehaviour"))
            {
                var tempList = targetLM.gameObject.GetComponentsInChildren<GameplayMonoBehaviour>(true);
                if (tempList != null)
                {
                    targetLM._GameplayGameObjects = new List<GameplayMonoBehaviour>(tempList);
                }
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }
        }
    }
}
