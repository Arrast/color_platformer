﻿using Singleton;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public class DataManager : Singleton<DataManager>
    {
        private Dictionary<System.Type, object> _DataStored;
        
        public override void Initialize()
        {
            base.Initialize();

            _DataStored = new Dictionary<System.Type, object>();
            LoadLevels();
        }

        private void LoadLevels()
        {
            LevelListScriptableObject levelList = Resources.Load<LevelListScriptableObject>("Data/LevelData");
            if (levelList != null)
            {
                List<Level> levels = new List<Level>(levelList.Levels);
                AddData<Level>(levels);

            }
            else
            {
                Debug.LogError("No Level List");
            }
        }

        public void AddData<T>(List<T> dataToAdd)
        {
            if (!_DataStored.ContainsKey(typeof(T)))
            {
                _DataStored.Add(typeof(T), dataToAdd);
            }
            else
            {
                Debug.LogErrorFormat("Already contains data of this type: {0}", typeof(T));
            }
        }

        public List<T> GetDataList<T>()
        {
            if (_DataStored.ContainsKey(typeof(T)))
            {
                return (List<T>)_DataStored[typeof(T)];
            }
            return null;
        }
    }
}
