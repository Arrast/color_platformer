﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAlphaOnTrigger : MonoBehaviour
{
    [SerializeField]
    private List<SpriteRenderer> _Sprites;

    [SerializeField]
    private float _NewAlpha;
       
    // We check collisions
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            ChangeAlpha(_NewAlpha);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            ChangeAlpha(1.0f);
        }
    }

    private void ChangeAlpha(float newAlpha)
    {
        for (int i = 0; i < _Sprites.Count; i++)
        {
            Color temp = _Sprites[i].color;
            temp.a = newAlpha;
            _Sprites[i].color = temp;
        }
    }
}
