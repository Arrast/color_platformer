﻿using UnityEngine;
using System.Collections;

public class PopupController : MonoBehaviour
{
	/// <summary>
	/// The scene model.
	/// </summary>
	public SceneModel sceneModel;

	protected virtual void Awake ()
	{
	}

    public virtual void Initialize(SceneModel sceneModel)
    {
        this.sceneModel = sceneModel; 
    }

    public virtual void OnBackButton()
	{
        Close();
    }

    public virtual void Close()
    {
        Destroy(gameObject);
    }
}
