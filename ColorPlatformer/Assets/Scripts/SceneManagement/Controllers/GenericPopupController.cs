﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GenericPopupController : PopupController
{
    [SerializeField]
    private TMP_Text _Title;

    [SerializeField]
    private TMP_Text _Body;

    public class GenericPopupConfig
    {
        public string Title;
        public string Body;
        public System.Action OnClose;
    }

    public override void Initialize(SceneModel sceneModel)
    {
        base.Initialize(sceneModel);
        if (sceneModel != null)
        {
            GenericPopupConfig config = (GenericPopupConfig)sceneModel.payload;

            if (_Title != null)
            {
                _Title.text = config.Title;
            }

            if (_Body != null)
            {
                _Body.text = config.Body;
            }
        }
    }

    public override void Close()
    {
        base.Close();
        if (sceneModel != null)
        {
            GenericPopupConfig config = (GenericPopupConfig)sceneModel.payload;
            if (config != null && config.OnClose != null)
            {
                config.OnClose();
            }
        }
    }
}
