﻿using ColorPlatformer.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ColorPlatformer
{
    public class MainMenuSceneController : SceneController
    {
        [SerializeField]
        private Transform _ButtonParent;

        public override void Initialize(SceneModel sceneModel)
        {
            base.Initialize(sceneModel);

            InitializeLevels();
        }

        public void InitializeLevels()
        {
            // TODO: Here we should handle whether the player has unlocked the level, and if they have already played it, show it somehow
            List<Level> levels = DataManager.Instance.GetDataList<Level>();
            if (levels != null)
            {
                LevelCellComponent cellPrefab = Resources.Load<LevelCellComponent>("LevelCell");
                for (int i = 0; i < levels.Count; i++)
                {
                    if(cellPrefab != null)
                    {
                        LevelCellComponent cellInstance = Instantiate< LevelCellComponent>(cellPrefab);
                        cellInstance.transform.SetParent(_ButtonParent, false);

                        LevelCellComponent.Config config = new LevelCellComponent.Config();
                        config.LevelData = levels[i];
                        config.OnButtonClicked = GoToScene;
                        cellInstance.Init(config);
                    }
                }
            }
        }

        private void GoToScene(string scene)
        {
            SceneTransitionManager.Instance.GoToScene(scene, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
    }
}
