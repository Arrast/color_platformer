﻿using UnityEngine;
using System.Collections;
using System;

public class SceneController : MonoBehaviour
{
	/// <summary>
	/// The scene model.
	/// </summary>
	public SceneModel sceneModel;

	protected virtual void Awake ()
	{
		
	}

	public virtual void OnBackButton()
	{
		//SceneTransitionManager.Instance.GoToScene (SCENES.MainMenu);
	}

    public virtual void Initialize(SceneModel sceneModel)
    {
        this.sceneModel = sceneModel;
    }
}
