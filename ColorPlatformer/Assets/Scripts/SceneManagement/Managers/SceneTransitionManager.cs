﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Singleton;

public class SceneModel
{
    public object payload = null;
    public string scene = "";

    public SceneModel() { }
    public SceneModel(string scene)
    {
        this.scene = scene;
    }

    public SceneModel(string scene, object payload)
    {
        this.scene = scene;
        this.payload = payload;
    }
}

public class SceneTransitionManager : Singleton<SceneTransitionManager>
{

    /// <summary>
    /// The scene model to send through scenes.
    /// </summary>
    public static SceneModel sceneModelToSend;

    public void GoToScene(string sceneToGo, LoadSceneMode sceneMode)
    {
        StartCoroutine(GoToSceneCoroutine(sceneToGo, sceneMode, null));
    }

    public void GoToScene(string sceneToGo, LoadSceneMode sceneMode, SceneModel sceneModel)
    {
        StartCoroutine(GoToSceneCoroutine(sceneToGo, sceneMode, sceneModel));
    }

    public void GoToMainMenu()
    {
        GoToScene(Constants.MAIN_MENU_SCENE, LoadSceneMode.Single);
    }

    private IEnumerator GoToSceneCoroutine(string sceneToGo, LoadSceneMode sceneMode, SceneModel sceneModel)
    {
        Debug.LogFormat("SceneTransitionManager - GoToScene:{0}", sceneToGo);
        if (sceneModel != null)
        {
            sceneModelToSend = sceneModel;
        }
        AsyncOperation loadSceneOperation = SceneManager.LoadSceneAsync(sceneToGo, sceneMode);
        yield return loadSceneOperation;

        //OnLevelLoaded();
    }

    //void OnLevelLoaded()
    //{
    //    SceneController sceneController = GameObject.FindObjectOfType<SceneController>();
    //    sceneController.Initialize(sceneModelToSend);

    //    sceneModelToSend = null;
    //}

    private void Start()
    {
        SceneController sceneController = FindObjectOfType<SceneController>();
        if (sceneController != null)
        {
            sceneController.Initialize(null);
        }

        SceneManager.sceneLoaded += SceneLoaded;
    }

    public void SceneLoaded(Scene sceneLoaded, LoadSceneMode sceneMode)
    {
        var gameObjects = sceneLoaded.GetRootGameObjects();
        SceneController sceneController = null;
        for (int i = 0; i < gameObjects.Length; i++)
        {
            sceneController = gameObjects[i].GetComponent<SceneController>();
            if (sceneController != null)
            {
                break;
            }
        }

        if (sceneController != null)
        {
            sceneController.Initialize(sceneModelToSend);
            sceneModelToSend = null;
        }
    }
}
