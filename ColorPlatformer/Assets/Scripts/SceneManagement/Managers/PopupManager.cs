﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Singleton;

public class PopupManager : Singleton<PopupManager>
{
    /// <summary>
    /// The scene model to send through scenes.
    /// </summary>
    public static SceneModel sceneModelToSend;

    //public void ShowPopup(SCENES scene)
    //{
    //    StartCoroutine("LoadLevelCoroutine", scene);
    //}

    //public void ShowPopup(SCENES scene, SceneModel popupSceneModel)
    //{
    //    PopupManager.sceneModelToSend = popupSceneModel;
    //    StartCoroutine("LoadLevelCoroutine", scene);
    //}

    //private IEnumerator LoadLevelCoroutine(SCENES scene)
    //{
    //    AsyncOperation async = SceneManager.LoadSceneAsync(scene.ToString(), LoadSceneMode.Additive);
    //    yield return async;

    //    OnLevelWasLoaded();
    //}

    //private void OnLevelWasLoaded() 
    //{
    //    SceneModel scm = PopupManager.sceneModelToSend;
    //    if (scm != null)
    //    {
    //        PopupController popupController = GameObject.FindObjectOfType<PopupController>();
    //        popupController.Initialize(scm);
    //    }

    //    PopupManager.sceneModelToSend = null;
    //}
}