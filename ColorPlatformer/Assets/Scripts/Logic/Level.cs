﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    [System.Serializable]
    public class Level
    {
        public string level_id;
        public string level_display_name;
        public string level_icon;
        public string screen_name;
        public float perfect_beat_time;
    }
}
