﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    [CreateAssetMenu(fileName = "LevelList", menuName = "Data/LevelList", order = 1)]
    public class LevelListScriptableObject : ScriptableObject
    {
        public List<Level> Levels;
    }
}
