using System;
using UnityEngine;

namespace ColorPlatformer
{
    public class CustomPlatformerCharacter2D : MonoBehaviour
    {
        public bool AllowMovement = true;

        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
        [SerializeField] private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.
        [SerializeField] private bool m_AirControl = false;                 // Whether or not a player can steer while jumping;
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

        private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
        const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
        private bool m_Grounded;            // Whether or not the player is grounded.
        private Rigidbody2D m_Rigidbody2D;

        private float m_CircleRadius = 0;
        private float m_JumpAmounts = 0;
        private void Awake()
        {
            // Setting up references.
            //m_GroundCheck = transform.Find("GroundCheck");
            var collider = gameObject.GetComponent<CircleCollider2D>();
            if (collider != null)
            {
                m_CircleRadius = collider.radius;
            }
            
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
        }


        private void FixedUpdate()
        {
            m_Grounded = false;

            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Vector3 groundCheckPosition = transform.position + Vector3.down * m_CircleRadius;
            Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheckPosition, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    m_JumpAmounts = 0;
                    m_Grounded = true;
                }
            }
        }


        public void Move(float move, bool jump)
        {
            if(!AllowMovement)
            {
                return;
            }

            //only control the player if grounded or airControl is turned on
            if (m_Grounded || m_AirControl)
            {
                // Move the character
                m_Rigidbody2D.velocity = new Vector2(move*m_MaxSpeed, m_Rigidbody2D.velocity.y);
            }

            // If the player should jump...
            if ((m_Grounded || m_JumpAmounts < 2) && jump)
            {
                // Add a vertical force to the player.
                m_Grounded = false;
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
                m_JumpAmounts++;
            }
        }
    }
}
