﻿using ColorPlatformer.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public class Rotator : GameplayMonoBehaviour
    {
        [SerializeField]
        private Vector3 _RotationAxis;

        [SerializeField]
        private float _Speed;

        // Update is called once per frame
        void Update()
        {
            float rotationAngle = _Speed * Time.deltaTime;
            transform.Rotate(_RotationAxis, rotationAngle);
        }
    }
}
