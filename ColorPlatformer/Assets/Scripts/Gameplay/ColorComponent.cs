﻿using ColorPlatformer.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public enum COLOR
    {
        NONE = 0,
        RED = 1,
        GREEN = 2,
        YELLOW = 3
    }

    public class ColorComponent : GameplayMonoBehaviour
    {
        public static int BASE_LAYER_INT = 7;

        [SerializeField]
        protected bool _InitializeOnAwake;

        [SerializeField]
        protected bool _UpdateName;

        [SerializeField]
        protected List<SpriteRenderer> _SpriteRenderer;

        [SerializeField]
        protected COLOR _CurrentColor = COLOR.YELLOW;

        [SerializeField]
        protected bool _ChangeLayer = true;

        public COLOR CurrentColor
        {
            get { return _CurrentColor; }
            set
            {
                _CurrentColor = value;
                UpdateColorAndLayer();
            }
        }

        public static Dictionary<COLOR, Color> COLORS_BY_ENUM = new Dictionary<COLOR, Color>()
        {
            { COLOR.NONE, new Color(0,0,0,0) },
            { COLOR.RED, new Color(1,0,0,1) },
            { COLOR.YELLOW, new Color(1,1,0,1) },
            { COLOR.GREEN, new Color(0,1,0,1) }
        };


        public virtual void UpdateColorAndLayer()
        {
            if (_SpriteRenderer != null)
            {
                for (int i = 0; i < _SpriteRenderer.Count; i++)
                {

                    _SpriteRenderer[i].color = COLORS_BY_ENUM[CurrentColor];
#if UNITY_EDITOR
                    if (_UpdateName)
                    {
                        _SpriteRenderer[i].gameObject.name = _SpriteRenderer[i].sprite.name + "_" + CurrentColor;
                    }
#endif
                    if (_ChangeLayer)
                    {
                        _SpriteRenderer[i].gameObject.layer = BASE_LAYER_INT + (int)_CurrentColor;
                    }
                }
            }

            if (_ChangeLayer)
            {
                gameObject.layer = BASE_LAYER_INT + (int)_CurrentColor;
            }
        }

        private void Awake()
        {
            if (_InitializeOnAwake)
            {
                UpdateColorAndLayer();
            }
        }

        public static int AddColors(COLOR color1, COLOR color2)
        {
            int result = (int)color1 | (int)color2;

            return result;
        }

        public static int SubstractColors(COLOR color1, COLOR color2)
        {
            int result = (int)color1 & (int)~color2;

            return result;
        }
    }

    public static class ColorExtensions
    {
        public static COLOR SubstractColor(this COLOR originalColor, COLOR colorToSubstract)
        {
            if (originalColor != COLOR.YELLOW)
            {
                return originalColor;
            }
            else
            {
                int result = (int)originalColor & (int)~colorToSubstract;
                return (COLOR)result;
            }
        }

        public static COLOR AddColor(this COLOR originalColor, COLOR colorToSubstract)
        {
            if (originalColor == COLOR.YELLOW)
            {
                return originalColor;
            }
            else
            {
                int result = (int)originalColor | (int)colorToSubstract;
                return (COLOR)result;
            }
        }
    }
}
