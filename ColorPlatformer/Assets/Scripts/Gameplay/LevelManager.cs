﻿using ColorPlatformer.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField]
        private COLOR _InitialColor;

        [SerializeField]
        private Transform _PlayerOrigin;

        public List<GameplayMonoBehaviour> _GameplayGameObjects;

        private CustomPlatformerCharacter2D _PlayerInstance;
        private ColorComponent _PlayerColorComponent;

        // Start is called before the first frame update
        void Start()
        {
            InitializeLevel();
        }

        public void InitializeLevel()
        {
            // Instantiate Player
            CustomPlatformerCharacter2D playerPrefab = Resources.Load<CustomPlatformerCharacter2D>("Player");
            if (playerPrefab != null)
            {
                _PlayerInstance = Instantiate(playerPrefab);
                ResetLevel();
            }

            // Set up Camera
            Camera2DFollow cameraScript = Camera.main.gameObject.GetComponent<Camera2DFollow>();
            if (cameraScript != null && _PlayerInstance != null)
            {
                cameraScript.SetTarget(_PlayerInstance.transform);
            }

            // Initialize the GameplayGameObjects
            if (_GameplayGameObjects != null)
            {
                for (int i = 0; i < _GameplayGameObjects.Count; i++)
                {
                    _GameplayGameObjects[i].Initialize(this);
                }
            }
        }

        public void ResetLevel()
        {
            if (_PlayerInstance != null)
            {
                _PlayerColorComponent = _PlayerInstance.GetComponent<ColorComponent>();
                if (_PlayerColorComponent != null)
                {
                    _PlayerColorComponent.CurrentColor = _InitialColor;
                }
                _PlayerInstance.transform.SetParent(_PlayerOrigin.transform.parent);
                _PlayerInstance.transform.position = _PlayerOrigin.position;
            }
        }

        public void WinLevel()
        {
            _PlayerInstance.AllowMovement = false;

            // Play the end sequence if needed
            StartCoroutine(WinLevelSequence())
;        }

        private IEnumerator WinLevelSequence()
        {
            // This will become some effect, animation or something... RN, just wait.
            yield return new WaitForSeconds(1);

            SceneTransitionManager.Instance.GoToScene("YouWinPopup", UnityEngine.SceneManagement.LoadSceneMode.Additive);
        }
    }
}
