﻿using ColorPlatformer.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public class KillComponent : GameplayMonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player" && _LevelManager != null)
            {
                _LevelManager.ResetLevel();
            }
        }
    }
}
