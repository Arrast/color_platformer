﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public class ColorParticleComponent : ColorComponent
    {
        [SerializeField]
        protected ParticleSystem _ParticleSystem;

        public override void UpdateColorAndLayer()
        {
            base.UpdateColorAndLayer();
            if (_ParticleSystem != null)
            {
                var main = _ParticleSystem.main;
                main.startColor = ColorComponent.COLORS_BY_ENUM[CurrentColor];

                // This may be only useful in Editor, since it shouldn't affect when you change things and then play
                ParticleSystem.Particle[] pSparticles = new ParticleSystem.Particle[_ParticleSystem.particleCount];
                _ParticleSystem.GetParticles(pSparticles);
                if (pSparticles.Length > 0)
                {
                    for (int i = 0; i < pSparticles.Length; i++)
                    {
                        pSparticles[i].startColor = ColorComponent.COLORS_BY_ENUM[CurrentColor];
                    }
                }

                _ParticleSystem.SetParticles(pSparticles);
            }
        }
    }
}
