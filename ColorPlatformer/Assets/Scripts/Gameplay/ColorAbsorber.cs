﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ColorPlatformer
{
    public class ColorAbsorber : ColorParticleComponent
    {
        [SerializeField]
        private Vector3 _RotationAxis;

        [SerializeField]
        private float _Speed;
        
        // Update is called once per frame
        void Update()
        {
            float rotationAngle = _Speed * Time.deltaTime;
            transform.Rotate(_RotationAxis, rotationAngle);
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                ColorComponent playerColorComponent = collision.gameObject.GetComponent<ColorComponent>();
                if (playerColorComponent != null)
                {
                    playerColorComponent.CurrentColor = playerColorComponent.CurrentColor.SubstractColor(CurrentColor);
                }
            }
        }
    }
}
