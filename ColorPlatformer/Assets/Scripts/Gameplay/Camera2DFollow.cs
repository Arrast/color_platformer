using ColorPlatformer.Gameplay;
using UnityEngine;

public class Camera2DFollow : GameplayMonoBehaviour
{
    public Transform target;
    public float OffsetY;

    public float damping = 1;
    public float lookAheadFactor = 3;
    public float lookAheadReturnSpeed = 0.5f;
    public float lookAheadMoveThreshold = 0.1f;

    private float _OffsetZ;
    private Vector3 m_LastTargetPosition;
    private Vector3 m_CurrentVelocity;
    private Vector3 m_LookAheadPos;

    [SerializeField]
    private float _MaxDistanceBeforeSnap;

    // Update is called once per frame
    private void Update()
    {
        if (target == null)
        {
            return;
        }

        if (Vector2.Distance(transform.position, (target.position + Vector3.up * OffsetY)) > _MaxDistanceBeforeSnap)
        {
            SnapToTarget();
        }
        else
        {
            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - m_LastTargetPosition).x;

            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;
            if (updateLookAheadTarget)
            {
                m_LookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);
            }

            Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward * _OffsetZ + Vector3.up * OffsetY;
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

            transform.position = newPos;
        }

        m_LastTargetPosition = target.position;
    }

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;

        // Now we set things up
        m_LastTargetPosition = target.position;
        _OffsetZ = (transform.position - target.position).z;
        transform.parent = null;

        SnapToTarget();
    }

    private void SnapToTarget()
    {
        Vector3 newPos = target.position + Vector3.forward * _OffsetZ + Vector3.up * OffsetY;
        transform.position = newPos;
    }
}
