﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer.Gameplay
{
    public class GameplayMonoBehaviour : MonoBehaviour
    {
        protected LevelManager _LevelManager;

        public virtual void Initialize(LevelManager levelManager)
        {
            _LevelManager = levelManager;
        }
    }
}
