﻿using ColorPlatformer.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public class TeleporterComponent : GameplayMonoBehaviour
    {
        [SerializeField]
        private TeleporterComponent _Destination;

        [SerializeField]
        private Vector3 _SpawnOffset;

        [HideInInspector]
        public bool TeleporterActive = true;

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (_Destination != null && collision.gameObject.tag == "Player" && TeleporterActive)
            {
                // We disable the teleporters
                _Destination.TeleporterActive = false;
                TeleporterActive = false;

                collision.transform.position = _Destination.transform.position + _SpawnOffset;
            }
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            if (_Destination != null && collision.gameObject.tag == "Player" && !TeleporterActive)
            {
                TeleporterActive = true;
            }
        }
    }
}
