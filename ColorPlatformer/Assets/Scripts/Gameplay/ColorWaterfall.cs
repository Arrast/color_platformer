﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public class ColorWaterfall : ColorComponent
    {
        [SerializeField]
        private Vector2 _AnimationAxis;

        [SerializeField]
        private float _Speed;

        protected Vector2 uvOffset;

        void Start()
        {
            uvOffset = new Vector2(0, 0);
        }

        // Update is called once per frame
        void Update()
        {
            if (_SpriteRenderer != null)
            {
                if (uvOffset.x >= 1.0f)
                {
                    uvOffset.x = 0.0f;
                }

                if (uvOffset.y >= 1.0f)
                {
                    uvOffset.y = 0.0f;
                }

                uvOffset += (_AnimationAxis * _Speed * Time.deltaTime);
                for (int i = 0; i < _SpriteRenderer.Count; i++)
                {
                    _SpriteRenderer[i].material.mainTextureOffset = uvOffset;
                }
            }
        }
        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                ColorComponent playerColorComponent = collision.gameObject.GetComponent<ColorComponent>();
                if (playerColorComponent != null)
                {
                    playerColorComponent.CurrentColor = playerColorComponent.CurrentColor.AddColor(CurrentColor);
                }
            }
        }
    }
}
