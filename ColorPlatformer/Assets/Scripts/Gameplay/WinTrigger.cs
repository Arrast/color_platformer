﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer.Gameplay
{ 
public class WinTrigger : GameplayMonoBehaviour
{
        private bool _Active = true;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (_Active && collision.gameObject.tag == "Player")
            {
                _Active = false;

                // Call the Level Manager
                _LevelManager.WinLevel();
            }
        }
    }
}
