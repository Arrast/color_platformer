﻿
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public static class AudioManagerEditor
{
    [MenuItem("VERSoft/Audio/Create AudioTriggerHolder")]
    public static void CreateGameLevelHolder()
    {
        AudioScriptableObject levelHolder = ScriptableObject.CreateInstance<AudioScriptableObject>();

        AssetDatabase.CreateAsset(levelHolder,
         "Assets/Resources/Audio/AudioManagerHolder.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = levelHolder;
    }
}