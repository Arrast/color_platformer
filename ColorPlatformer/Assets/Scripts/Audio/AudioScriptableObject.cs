﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioScriptableObject : ScriptableObject
{
    /*/
    public Dictionary<AudioPlayableTrigger, List<AudioClip>> _dictionary;
    public AudioManager(Dictionary<AudioPlayableTrigger, List<AudioClip>> dict)
    {
        _dictionary = dict;
    }
    //*/
    public List<AudioScriptableObjectItem> _list;
    public AudioScriptableObject(List<AudioScriptableObjectItem> list)
    {
        _list = list;
    }
}

[System.Serializable]
public class AudioScriptableObjectItem
{
    public AudioPlayableTrigger _trigger;
    public List<AudioClip> _soundFiles;
}
