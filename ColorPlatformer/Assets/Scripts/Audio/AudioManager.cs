﻿
using UnityEngine;
using System.Collections;
using System.Linq;
using Singleton;
using System.Collections.Generic;
//using Storage;

public class AudioManager : Singleton<AudioManager>
{
    public bool _Initialized = false;
    public AudioScriptableObject _object;
    public List<AudioSource> sfxAudioSources;
    public AudioSource musicAudioSource;

    public float _MusicDefaultVolume = 0.3f;
    public float _SFXDefaultVolume = 1.0f;

    public override void Initialize()
    {
        if (_Initialized)
        {
            return;
        }

        _Initialized = true;

        // We instantiate the Music Audio Source (UNIQUE)
        InstantiateMusicAudioSource();
        sfxAudioSources = new List<AudioSource>();

        _object = Resources.Load<AudioScriptableObject>("Audio/AudioManagerHolder");
    }

    private void InstantiateMusicAudioSource()
    {
        musicAudioSource = Instance.gameObject.AddComponent<AudioSource>();
        musicAudioSource.playOnAwake = false;
        musicAudioSource.loop = true;
    }

    private AudioSource InstantiateAudioSource()
    {
        var audioSource = Instance.gameObject.AddComponent<AudioSource>();
        audioSource.playOnAwake = false;

        return audioSource;
    }

    private AudioSource GetAudioSource()
    {
        int index = 0;
        for (; index < sfxAudioSources.Count; index++)
        {
            // If it's not playing, return it
            if (!sfxAudioSources[index].isPlaying)
            {
                break;
            }
        }

        if (index == sfxAudioSources.Count)
        {
            sfxAudioSources.Add(InstantiateAudioSource());
        }

        return sfxAudioSources[index];
    }

    public void PlaySFX(AudioPlayableTrigger trigger, float volume = -1.0f)
    {
        if (_object == null || _object._list == null || _object._list.Count == 0)
            return;

        var audioSource = GetAudioSource();
        if (audioSource != null)
        {
            AudioScriptableObjectItem item = _object._list.Where(x => x._trigger == trigger).First();
            int selectedClip = Random.Range(0, item._soundFiles.Count);
            AudioClip clip = item._soundFiles[selectedClip];

            if (volume < 0)
            {
                volume = _SFXDefaultVolume;
            }
            audioSource.volume = volume;
            audioSource.PlayOneShot(clip);
        }
    }

    public void PlayMusic(AudioPlayableTrigger trigger, float volume = -1.0f)
    {
        musicAudioSource.Stop();
        AudioScriptableObjectItem item = _object._list.Where(x => x._trigger == trigger).First();
        AudioClip clip = item._soundFiles[Random.Range(0, item._soundFiles.Count - 1)];

        if (volume < 0)
        {
            volume = _MusicDefaultVolume;
        }
        musicAudioSource.volume = volume;
        musicAudioSource.clip = clip;
        musicAudioSource.Play();
    }

    public bool IsMusicPlaying()
    {
        return musicAudioSource != null && musicAudioSource.isPlaying;
    }
}
