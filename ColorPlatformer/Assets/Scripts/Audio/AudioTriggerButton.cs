﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Audio
{
    [RequireComponent (typeof(Button))]
    public class AudioTriggerButton : MonoBehaviour 
    {
        [SerializeField] private Button button;
        [SerializeField] private AudioPlayableTrigger audioTrigger;

        private void Awake()
        {
            // If we don't have a button, we try to get one
            if (button == null)
            {
                button = gameObject.GetComponent<Button>();
            }

            if (button != null)
            {            
                button.onClick.AddListener(PlayAudio);
            }
        }

        private void PlayAudio()
        {
            AudioManager.Instance.PlaySFX(audioTrigger);
        }
    }
}
