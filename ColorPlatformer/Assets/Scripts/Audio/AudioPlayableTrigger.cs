﻿using UnityEngine;
using System.Collections;

public enum AudioPlayableTrigger
{
    Music = 0,
    Click = 1,
    Splatter = 2,
    CowMoo = 3,
    WaterSplash = 4,
    MudSplash = 5
}
