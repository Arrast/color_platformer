﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ColorPlatformer.UI
{
    public class LevelCellComponent : MonoBehaviour
    {
        public class Config
        {
            public Level LevelData;
            public System.Action<string> OnButtonClicked;
        }

        [SerializeField]
        private TMP_Text _LevelName;


        private Config _Config;

        public void Init(Config config)
        {
            _Config = config;
            if (_Config != null)
            {
                if (_LevelName != null)
                {
                    _LevelName.text = _Config.LevelData.level_display_name;
                }
            }
        }

        public void OnClick()
        {
            if (_Config != null)
            {
                _Config.OnButtonClicked(_Config.LevelData.screen_name);
            }
        }
    }
}
