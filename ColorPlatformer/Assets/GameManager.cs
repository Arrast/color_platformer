﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorPlatformer
{
    public class GameManager : MonoBehaviour
    {
        private void Awake()
        {
            DataManager.Instance.Initialize();
            SceneTransitionManager.Instance.Initialize();
        }
    }
}
