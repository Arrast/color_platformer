﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenController : SceneController
{
    // Start is called before the first frame update
    public IEnumerator GoToMainMenu()
    {
        yield return new WaitForSeconds(1);
        SceneTransitionManager.Instance.GoToMainMenu();
    }

    public override void Initialize(SceneModel sceneModel)
    {
        base.Initialize(sceneModel);
        StartCoroutine(GoToMainMenu());
    }
}
