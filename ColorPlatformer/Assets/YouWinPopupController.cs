﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YouWinPopupController : SceneController
{
    public void OnMainMenuButtonClicked()
    {
        SceneTransitionManager.Instance.GoToMainMenu();
    }
}
